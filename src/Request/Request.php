<?php
namespace Framework\Http\Request;

final class Request
{

    private static $params = [];

    public function __construct()
    {
        
    }

    /**
     * Set custom parameters to this requet
     * @param array $param
     * @return Request
     */
    public function setParams(array $param)
    {
        self::$params = array_merge(self::$params, $param);

        return $this;
    }

    /**
     * Get desired parameter of this request
     * @param array|string|null $param
     * @return boolean
     */
    public function getParams($param = null)
    {
        if (is_array($param)) {
            /**
             * We will create temporary array
             * for multiple params
             */
            $tempParam = [];
            foreach ($param as $key => $value) {
                if (isset(self::$params[$value])) {
                    $tempParam[$value] = self::$params[$value];
                }
            }

            return $tempParam;
        } elseif ($param !== null && isset(self::$params[$param])) {

            return self::$params[$param];
        }

        return false;
    }

    public function getUrlData($key = false)
    {
        return $key === false ? $_GET : ($_GET[$key] ?? false);
    }

    public function getPostData($key = false)
    {
        return $key === false ? $_POST : ($_POST[$key] ?? false);
    }

    /**
     * @deprecated since version 4
     * @return string
     */
    public function requestType()
    {
        return strtolower($_SERVER["REQUEST_METHOD"] ?? 'GET');
    }

    public function requestMethod()
    {
        return strtoupper($_SERVER["REQUEST_METHOD"] ?? 'GET');
    }

    public function isPost()
    {
        return $this->requestMethod() === 'POST';
    }

    public function isGet()
    {
        return $this->requestMethod() === 'GET';
    }

    /**
     * Detect order column for this query
     * @param \Framework\Database\Stacky\Model $model
     * @param string $default
     * @return string
     */
    public function getOrder(\Framework\Database\Stacky\Model $model, string $default = 'id')
    {
        if (!empty($_GET['order'])) {
            $order = strtolower($_GET['order']);
            if ($model->isColumn($order)) {
                return $order;
            }
        }
        return $default;
    }

    /**
     * Return Sort direction 
     * @param string $default
     * @return string
     */
    public function getSort(string $default = 'ASC')
    {
        if (!empty($_GET['sort'])) {
            $sort = strtoupper($_GET['sort']);
            if ($sort !== 'DESC') {
                $sort = 'ASC';
            }

            return $sort;
        }
        return $default;
    }

    /**
     * Return content limit for each page
     * @param int $default
     * @return int
     */
    public function getPerPage(int $default = 10)
    {
        if (!empty($_GET['perPage'])) {
            $perPage = (int) $_GET['perPage'];

            return $perPage;
        }
        return $default;
    }
}
