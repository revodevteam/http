<?php
namespace Framework\Http\Session;

class Session implements \ArrayAccess, \Countable
{

    public function set($name, $value)
    {
        $_SESSION[$name] = $value;

        return $this->is($name);
    }

    public function append($name, $value)
    {
        if ($this->is($name)) {            
            if (is_array($this->get($name)) && is_array($value)) {
                $this->set($name, array_merge($this->get($name), $value));
            } elseif (is_array($this->get($name)) && is_array($value) === false) {
                $this->set($name, array_push($this->get($name), $value));
            } else {
                $this->set($name, $value);
            }
        } else {
            $this->set($name, $value);
        }

        return $this->is($name);
    }

    public function remove($name)
    {
        if ($this->is($name)) {
            unset($_SESSION[$name]);
        }
    }

    public function get($name)
    {
        if ($this->is($name)) {
            return $_SESSION[$name];
        }
        return false;
    }

    public function is($name)
    {
        return array_key_exists($name, $_SESSION);
    }

    public function has($name)
    {
        return isset($_SESSION[$name]);
    }

    public function offsetSet($offset, $value)
    {
        if (is_string($offset)) {
            $this->set($offset, $value);
        } else {
            throw new \Exception('Please specify key');
        }
    }

    public function offsetExists($offset)
    {
        return $this->is($offset);
    }

    public function offsetUnset($offset)
    {
        $this->remove($offset);
    }

    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    public function count(): int
    {
        return count($_SESSION);
    }
}
