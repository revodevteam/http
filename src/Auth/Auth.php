<?php
namespace Framework\Http\Auth;

use Framework\Http\Session\Session;
use Framework\Database\Stacky\Model;

class Auth extends Session
{

    public function setLoggedIn($uid, $umail, $type = 'user')
    {
        $this->set(SITE . '_' . $type . '_id', $uid);
        $this->set(SITE . '_' . $type . '_mail', $umail);
    }

    public function setLoggedOut($type = 'user')
    {
        $this->remove(SITE . '_' . $type . '_id');
        $this->remove(SITE . '_' . $type . '_mail');
    }

    public function isLoggedIn($type = 'user')
    {
        return $this->is(SITE . '_' . $type . '_id') &&
            $this->is(SITE . '_' . $type . '_mail');
    }

    public function loggedID($type = 'user')
    {
        return (int) $this->get(SITE . '_' . $type . '_id');
    }

    public function loggedEMAIL($type = 'user')
    {
        return $this->get(SITE . '_' . $type . '_mail');
    }

    protected function validateAdmin(Model $model, int $id, int $role = null, string $destination = '')
    {
        if (($admin = $model->findByID($id)) !== false) {
            if ($role === null || $admin->role >= $role) {
                return true;
            }
        }

        redirect($destination);
    }
}
