<?php

namespace Framework\Http\Uploader;

use Framework\String\Str;

class File
{

    protected $file;
    protected $target;

    public function __construct($file)
    {
        if (!empty($file)) {
            $this->file = (object) $file;
            $this->target = (object) array('name' => $this->file->name);

            $this->details();
        } else {
            $this->file = (object) array('error' => 4);
        }
    }

    public function __get($name)
    {
        if (property_exists($this->file, $name)) {
            return $this->file->{$name};
        }

        return false;
    }

    public function details()
    {
        $this->file->temp_path = $this->file->tmp_name;
        $this->file->basename = pathinfo($this->file->name, PATHINFO_BASENAME);
        $this->file->extension = pathinfo($this->file->name, PATHINFO_EXTENSION);
    }

    public function rename(string $name)
    {
        $this->target->name = $name;

        return $this;
    }

    public function save($destinationDIR)
    {
        $fileName = Str::alphaNum(pathinfo($this->target->name, PATHINFO_FILENAME));
        $fileExt = pathinfo($this->target->name, PATHINFO_EXTENSION);
        while (file_exists($destinationDIR . DS . $fileName . '.' . $fileExt) === true) {
            $fileName .= '-1';
        }

        $destinationPath = $destinationDIR . DS . $fileName . '.' . $fileExt;

        if (move_uploaded_file($this->file->temp_path, $destinationPath)) {
            return $destinationPath;
        }

        return false;
    }

    public function isImage()
    {
        $imgExt = array('jpg', 'jpeg', 'png');

        return in_array(strtolower($this->file->extension), $imgExt);
    }

    public function isValid()
    {
        return $this->file->error === 0;
    }

}
