<?php

namespace Framework\Http\Uploader;

class Uploader
{

    protected $files;

    public function __construct()
    {
        $this->files = $_FILES;
    }

    public function validRequest()
    {
        if (is_array($this->files) && !empty($this->files)) {
            return true;
        }

        return false;
    }

    public function file(string $field)
    {
        if (array_key_exists($field, $this->files)) {
            if (is_array($this->files[$field]['name'])) {
                $files = [];
                foreach ($this->files[$field]['name'] as $key => $fileName) {
                    $file = [];
                    $file['name'] = $fileName;
                    $file['type'] = $this->files[$field]['type'][$key];
                    $file['tmp_name'] = $this->files[$field]['tmp_name'][$key];
                    $file['error'] = $this->files[$field]['error'][$key];
                    $file['size'] = $this->files[$field]['size'][$key];

                    $files[] = new File($file);
                }

                return $files;
            } else {
                return new File($this->files[$field]);
            }
        }

        return new File(false);
    }

}
