<?php
namespace Framework\Http\Routing;

use Framework\DInjector\Singleton;
use Framework\Http\Request\Request;
use Framework\Http\Session\Session;

class Router extends RouterHelper
{

    const RESOURCE_ROUTES = [
        ['route' => '', 'method' => 'showList'],
        ['route' => '/{id:\d+}', 'method' => 'showSingle'],
        ['route' => '/add', 'method' => 'create'],
        ['route' => '/edit/{id:\d+}', 'method' => 'edit'],
        ['route' => '/delete/{id:\d+}', 'method' => 'delete']
    ];

    public function __construct()
    {
        $this->setDomain();
        $this->currentURL();
        $this->detectHttpRequest();
        $this->routeCollection = [];
        //$this->routeIndex = -1;
        $this->request = Singleton::setInstance(new Request(new Session()));
    }

    /**
     * We add specific route to our collection
     * @param \Framework\Http\Routing\Route $route
     */
    protected function addToCollection(Route $route)
    {
        $this->routeCollection[$route->route] = $route;
        $this->routeIndex = $route->route;
    }

    /**
     * Add route by http verb and url
     * @param array $httpVerb
     * @param string $route
     */
    public function add(array $httpVerb, string $route)
    {
        //set prefix if exists
        $route = empty($this->prefix) ? $route : $this->prefix . $route;

        //Check if that dumb user has inserted a slash or anything at all !
        if (empty($route) || $route[0] !== '/') {
            throw new RouteException('Route must have a slash (/) as first character !');
        }
        //Set route according to domain
        $route = $this->cleanRoute($this->domain . $route);

        $routeObj = new Route();
        $routeObj->route = $this->regularExpression($route);
        $routeObj->http = $httpVerb;
        $routeObj->middlewares = $this->middlewares ?? [];

        $this->addToCollection($routeObj);

        return $this;
    }

    /**
     * We can group multiple route by same prefix
     * @param array $options
     * @param closure $callback
     */
    public function group(array $options, $callback)
    {
        $backup = [];
        if (isset($options['domain'])) {
            $backup['domain'] = $this->domain;
            $this->setDomain($options['domain']);
        }
        if (isset($options['prefix'])) {
            $backup['prefix'] = $this->prefix;
            $this->setPrefix($options['prefix']);
        }
        if (isset($options['middlewares'])) {
            $backup['middlewares'] = $this->middlewares;
            $this->middlewares = $options['middlewares'];
        }

        $this->callBack($callback, $this);

        // Restore Old Params
        if (isset($options['domain'])) {
            $this->setDomain($backup['domain']);
        }
        if (isset($options['prefix'])) {
            $this->setPrefix($backup['prefix']);
        }
        if (isset($options['middlewares'])) {
            $this->middlewares = $backup['middlewares'];
        }
    }

    public function resource(string $route, string $controller, $middlewares = [])
    {
        foreach (self::RESOURCE_ROUTES as $i => $resource) {
            /**
             * If its first route, consider it as a list page, so pluralize route link
             * Otherwise, use default route as provided
             */
            $url = (($i === 0) ? $this->getPluralizeRoute($route) : $route) . $resource['route'];
            $method = $resource['method'];

            $this->add([], $url)->action($controller, $method)->middleware($middlewares);
        }
    }

    protected function returnCollection()
    {
        if (!empty($this->routeCollection)) {
            return $this->routeCollection;
        }
        throw new RouteException('No Route Found');
    }

    public function flushCollection()
    {
        unset($this->routeCollection);
    }

    /**
     * 
     * @param string $url
     * @return boolean
     */
    protected function match($url, $http = 'GET')
    {
        //Try with static route
        if ($this->exactMatch($url, $http) === true) {
            return true;
        }

        // Loop through all route list
        foreach ($this->returnCollection() as $route) {
            if ((empty($route->http) || in_array($http, $route->http)) && preg_match($route->route, $url, $matches)) {

                $route->parameters = array_filter($matches, function($key) {
                    return is_string($key);
                }, ARRAY_FILTER_USE_KEY);

                $this->request->setParams($route->parameters);

                $this->matchedRoute = $route;

                return true;
            }
        }
        return false;
    }

    protected function exactMatch($url, $http)
    {
        $pregURL = '~^' . $url . '$~';

        if (isset($this->routeCollection[$pregURL]) && ((empty($this->routeCollection[$pregURL]->http) || in_array($http, $this->routeCollection[$pregURL]->http)))) {
            $this->matchedRoute = $this->routeCollection[$pregURL];
            return true;
        }
        return false;
    }

    /**
     * It's time to dispatch routes
     * @param closure|boolean $successCallBack
     * @param closure|boolean $failureCallBack
     * @return Router
     */
    public function dispatch($successCallBack = false, $failureCallBack = false)
    {

        if ($this->match($this->currentURL, $this->httpRequest) !== false) {
            if (!empty($this->matchedRoute->callback) && is_callable($this->matchedRoute->callback)) {
                $successCallBack = $this->matchedRoute->callback;

                $parameters['parameters']['callBackResult'] = $this->callBack($successCallBack, $this->matchedRoute->parameters);
            } elseif (!empty($successCallBack) && is_callable($successCallBack)) {
                $parameters['parameters']['callBackResult'] = $this->callBack($successCallBack, $this->matchedRoute->parameters);
            }

            return $this;
        } elseif (!empty($failureCallBack)) {
            $this->callBack($failureCallBack, $this->currentURL);
        } else {
            throw new RouteException('"' . $this->currentURL . '" Route not found !');
        }
    }
}
