<?php
namespace Framework\Http\Routing;

use Framework\Cache\Cachier;
use Inflect\Inflect;

abstract class RouterHelper
{

    protected $routeCollection;
    protected $httpRequest;
    protected $matchedRoute;
    protected $domain;
    protected $currentURL;
    protected $routeIndex;

    /**
     * To be used in Group
     */
    protected $prefix;
    protected $middlewares;

    protected function getPluralizeRoute($route)
    {
        $routeWord = strstr($route, '/');
        $plural = Inflect::pluralize($routeWord);

        return strstr($route, '/', true) . $plural;
    }

    public function getRoutes()
    {
        return $this->routeCollection;
    }

    public function getMatchedRoute()
    {
        return (object) $this->matchedRoute;
    }

    public function setDomain($url = null)
    {
        if (empty($url)) {
            $url = BASE_URL;
        }


        $this->domain = trim($url, '/');
    }

    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    public function removePrefix()
    {
        $this->prefix = '';
    }

    public function prefix($prefix)
    {
        $this->prefix = $prefix;
        return $this;
    }

    public function domain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    public function currentURL()
    {
        $this->currentURL = $this->getSchema() . $this->getDomain() . $this->getURI();
    }

    protected function getSchema()
    {
        if (isSSL()) {
            return 'https://';
        }
        return 'http://';
    }

    protected function getDomain()
    {
        if (isset($_SERVER['HTTP_HOST'])) {
            return $_SERVER["HTTP_HOST"];
        }
        return '';
    }

    protected function getURI()
    {
        if (isset($_SERVER['REQUEST_URI'])) {
            return $this->cleanUrl($_SERVER["REQUEST_URI"]);
        }
        return '';
    }

    protected function detectHttpRequest()
    {
        if (!empty($_SERVER) && isset($_SERVER['REQUEST_METHOD'])) {
            return $this->httpRequest = strtoupper($_SERVER['REQUEST_METHOD']);
        }

        return $this->httpRequest = 'GET';
    }

    protected function cleanUrl($url)
    {
        return $this->cleanRoute(current(explode('?', $url)));
    }

    protected function cleanRoute($route)
    {
        return rtrim($route, '/');
    }

    protected function callBack($callback, ...$params)
    {
        if (is_callable($callback)) {
            return $callback(...$params);
        }
        throw new RouteException("Invalid call function");
    }

    /**
     * 
     * @param callable $callback
     * @param int $ttl
     * @return $this
     */
    public function enableCache(callable $callback, int $ttl = 3600)
    {
        $cachier = new Cachier();
        $cacheKey = 'route-collection';

        if (($cachedCollection = $cachier->get($cacheKey, $ttl)) !== false) {
            if (empty($this->routeCollection)) {
                $this->routeCollection = $cachedCollection;
            } else {
                $this->routeCollection = array_merge($this->routeCollection, $cachedCollection);
            }
            return $this;
        }

        $this->callBack($callback, $this);

        if (!empty($this->routeCollection)) {
            $cachier->set($cacheKey, $this->routeCollection, $ttl);
        }

        return $this;
    }

    /**
     * Check if Current Index exists in collection
     * @param type $index
     * @return boolean
     * @throws \Exception
     */
    protected function isValid($index)
    {
        if (isset($this->routeCollection[$index])) {
            return true;
        }

        throw new \Exception('Invalid Route Requested');
    }

    /**
     * Assign Action to a route entry
     * @param string $controller
     * @param string $method
     * @return $this
     */
    public function action(string $controller, string $method)
    {
        if ($this->isValid($this->routeIndex)) {
            $this->routeCollection[$this->routeIndex]->controller = $controller;
            $this->routeCollection[$this->routeIndex]->method = $method;
        }

        return $this;
    }

    /**
     * Assign middleware pool to a route entry
     * @param array $middlewares
     * @return $this
     */
    public function middleware(array $middlewares, bool $overWrite = false)
    {
        if ($this->isValid($this->routeIndex) === false) {
            return $this;
        }

        if (!empty($this->routeCollection[$this->routeIndex]->middlewares)) {
            $this->routeCollection[$this->routeIndex]->middlewares = ($overWrite === false ? array_merge($this->routeCollection[$this->routeIndex]->middlewares, $middlewares) : $middlewares);
        } else {
            $this->routeCollection[$this->routeIndex]->middlewares = $middlewares;
        }

        return $this;
    }

    public function onSuccess(callable $callback)
    {
        if ($this->isValid($this->routeIndex)) {
            $this->routeCollection[$this->routeIndex]->callback = $callback;
        }

        return $this;
    }

    protected function regularExpression(string $route)
    {
        $route = $this->easyRegEx($route);
        $route = preg_replace("/\{([a-zA-Z0-9\_]+):([^\}]+)\}/", "(?P<$1>$2)", $route);
        $route = '~^' . $route . '$~';

        return $route;
    }

    protected function easyRegEx(string $route)
    {
        $easyRegExArray = array(':int}', ':word}', ':string}');
        $regExArray = array(':[\d^/]+}', ':[\w^/]}', ':[\S^/]+}');

        return str_replace($easyRegExArray, $regExArray, $route);
    }

    /**
     * 
     * @param string $route
     * @return \Framework\Http\Routing\Router
     */
    public function any(string $route): Router
    {
        return $this->add([], $route);
    }

    /**
     * 
     * @param string $route
     * @return Router
     */
    public function get(string $route)
    {
        return $this->add(['GET'], $route);
    }

    /**
     * 
     * @param string $route
     * @return Router
     */
    public function post(string $route)
    {
        return $this->add(['POST'], $route);
    }

    /**
     * 
     * @param string $route
     * @return Router
     */
    public function put(string $route)
    {
        return $this->add(['PUT'], $route);
    }

    /**
     * 
     * @param string $route
     * @return Router
     */
    public function delete(string $route)
    {
        return $this->add(['DELETE'], $route);
    }

    /**
     * 
     * @param string $route
     * @return Router
     */
    public function patch(string $route)
    {
        return $this->add(['PATCH'], $route);
    }

    /**
     * 
     * @param string $route
     * @return Router
     */
    public function head(string $route)
    {
        return $this->add(['HEAD'], $route);
    }

    /**
     * 
     * @param string $route
     * @return Router
     */
    public function options(string $route)
    {
        return $this->add(['OPTIONS'], $route);
    }
}
