<?php
namespace Framework\Http\Notification;

use Framework\Http\Notification\Notes\NoteInterface;

interface NotificationInterface
{

    public function hasNote(): bool;

    public function countNote(): int;

    public function setNote(NoteInterface $note): bool;

    public function flashNote();

    public function noteFactory(): NoteInterface;

    public function successNote(string $message);

    public function warningNote(string $message);

    public function errorNote(string $message);
}
