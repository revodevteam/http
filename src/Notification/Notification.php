<?php
namespace Framework\Http\Notification;

use Framework\Http\Session\Session;
use Framework\Http\Notification\Notes\BootstrapNote;
use Framework\Http\Notification\Notes\NoteInterface;
use Framework\Http\Notification\NotificationInterface;

final class Notification extends Session implements NotificationInterface
{

    private $noteClass;

    public function __construct(string $noteClass = null)
    {
        $noteClass = $noteClass ?: BootstrapNote ::class;

        $this->noteClass = $noteClass;
    }

    public function hasNote(): bool
    {
        return $this->has('flash_notification');
    }

    public function countNote(): int
    {

        if ($this->has('flash_notification')) {
            return count($this->get('flash_notification'));
        }

        return 0;
    }

    public function setNote(NoteInterface $note): bool
    {
        if ($this->hasNote()) {
            $notifications = $this->get('flash_notification');
            $notifications[] = $note;
            $this->set('flash_notification', $notifications);
        } else {
            $this->set('flash_notification', [$note]);
        }

        return $this->hasNote();
    }

    public function flashNote()
    {
        if ($this->hasNote()) {
            $notifications = $this->get('flash_notification');

            $this->remove('flash_notification');

            return $notifications;
        }

        return false;
    }

    /**
     * 
     * @return NoteInterface
     */
    public function noteFactory(): NoteInterface
    {
        return new $this->noteClass;
    }

    public function successNote(string $message)
    {
        $note = $this->noteFactory();
        $note->message = $message;
        $note->markAsSuccess();

        $this->setNote($note);
    }

    public function warningNote(string $message)
    {
        $note = $this->noteFactory();
        $note->message = $message;
        $note->markAsWarning();

        $this->setNote($note);
    }

    public function errorNote(string $message)
    {
        $note = $this->noteFactory();
        $note->message = $message;
        $note->markAsError();

        $this->setNote($note);
    }
}
