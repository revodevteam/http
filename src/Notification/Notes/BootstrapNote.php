<?php
namespace Framework\Http\Notification\Notes;

class BootstrapNote extends SimpleNote implements NoteInterface
{

    public $icon;

    public function markAsSuccess()
    {
        $this->type = 'success';
        $this->icon = 'check';
    }

    public function markAsWarning()
    {
        $this->type = 'warning';
        $this->icon = 'exclamation-triangle';
    }

    public function markAsError()
    {
        $this->type = 'danger';
        $this->icon = 'times';
    }
}
