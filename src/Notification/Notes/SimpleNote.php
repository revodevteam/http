<?php
namespace Framework\Http\Notification\Notes;

class SimpleNote implements NoteInterface
{

    public $message, $type;

    public function markAsError()
    {
        $this->type = 'error';
    }

    public function markAsSuccess()
    {
        $this->type = 'success';
    }

    public function markAsWarning()
    {
        $this->type = 'warning';
    }
}
