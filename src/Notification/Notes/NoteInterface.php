<?php
namespace Framework\Http\Notification\Notes;

interface NoteInterface
{

    public function markAsSuccess();

    public function markAsWarning();

    public function markAsError();
}
