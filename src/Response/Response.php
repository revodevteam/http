<?php

namespace Framework\Http\Response;

class Response
{

    public function __construct()
    {
        
    }

    public static function json($content)
    {
        if (((!empty($_SERVER['HTTP_X_REQUESTED_WITH'])) && ($_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest')) ||
                ((!empty($_SERVER['HTTP_ACCEPT'])) && ($_SERVER['HTTP_ACCEPT'] === 'application/json'))) {
            //All is Well
            //Lets Roll
            header('Content-Type: application/json');
            echo json_encode($content);
            exit;
        }
    }

    public static function xml($content)
    {
        header('Content-type: application/xml');
        echo $content;
        exit;
    }

    public static function throwFile($file, $force = false)
    {
        if (is_readable($file) === false) {
            throw new \Exception($file . ' not found !');
        }

        header('Content-Length: ' . filesize($file));
        header('Cache-Control: no-cache, no-store, must-revalidate');
        header('Pragma: no-cache');
        header('Expires: 0');

        if ($force === true) {
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        } else {
            header('Content-Type: ' . mime_content_type($file));
        }

        readfile($file);
        exit;
    }

}
