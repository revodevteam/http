<?php

namespace Framework\Http\Response;

use Framework\Http\Notification\JsonNotification;

class Json {

    public static function respond($body, JsonNotification $jsonNotification, $headers = [])
    {
        header('Content-Type: application/json');

        $content = ['body' => $body, 'report' => $jsonNotification->returnNotes()];

        echo json_encode($content);
        exit;
    }

}
