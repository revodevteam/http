<?php

namespace Framework\Http\MiddleWare;

use Closure;
use Framework\Http\Request\Request;

class CleanPost implements MiddleWareInterface {

    public function handle(Request $request, Closure $next)
    {
        if ($request->isPost()) {
            $_POST = safeTrim($_POST);
        }


        return $next($request);
    }

}
