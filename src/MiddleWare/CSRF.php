<?php
namespace Framework\Http\MiddleWare;

use Closure;
use Framework\Http\Request\Request;
use Framework\Exception\InvalidCsrfTokenException;

class CSRF implements MiddleWareInterface
{

    protected $session;

    public function __construct(\Framework\Http\Session\Session $session)
    {
        $this->session = $session;
    }

    public function handle(Request $request, Closure $next)
    {
        if ($request->isPost()) {
            $token = $_POST['csrf_token'] ?? $_GET['csrf_token'] ?? false;

            if ($this->session->is('csrf_token') &&
                !empty($token) &&
                $this->session->get('csrf_token') === $token) {

                // Check post done
                return $next($request);
            }

            throw new InvalidCsrfTokenException;
        } elseif (isset($this->session['csrf_token']) === false) {
            $this->session->set('csrf_token', bin2hex(random_bytes(32)));
        }
        
        return $next($request);
    }
}
