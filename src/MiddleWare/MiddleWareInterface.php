<?php

namespace Framework\Http\MiddleWare;

use Closure;
use Framework\Http\Request\Request;

interface MiddleWareInterface {

    public function handle(Request $request, Closure $next);
}
