<?php

namespace Framework\Http\MiddleWare;

use Closure;
use Framework\Http\Request\Request;

class Session implements MiddleWareInterface {

    public function handle(Request $request, Closure $next)
    {
        session_start();

        return $next($request);
    }

}
